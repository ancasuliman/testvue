import Vue from "vue";
import * as Sentry from "@sentry/browser";
import { Integrations } from "@sentry/tracing";
import App from "./App.vue";

Sentry.init({
    Vue,
    dsn:
        "https://5a4106a58ebe40dda6c583d477d2cdd6@o488150.ingest.sentry.io/5547978",
    integrations: [new Integrations.BrowserTracing()],

    // We recommend adjusting this value in production, or using tracesSampler
    // for finer control
    tracesSampleRate: 1.0,
});

Vue.config.productionTip = false;

new Vue({
    render: (h) => h(App),
}).$mount("#app");
